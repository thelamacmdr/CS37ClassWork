#include <iostream>
#include <iomanip>
using namespace std;

const int MAX_NAME_SIZE = 50;
const int MAX_HW_SIZE   = 3;
const int MAX_EXAM_SIZE = 2;
const int BUFFER_SIZE = 1000;
//Prototypes
struct studentRec
{
		                        
		char name[MAX_NAME_SIZE];
        int hw[MAX_HW_SIZE];
        int exam[MAX_EXAM_SIZE];
		float totalHomeWkPts;
		float totalExamPts;
        float classPert;
		char grade;
};

studentRec getStudentData();
void calculateStuGrade(studentRec &student);
void outputStuData (studentRec &student);
void outputAveExams (studentRec student [], int totalStudent);
void outputTotalGrades (studentRec student [], int totalStudent);


int main()

{
	const int TOTAL_STUDENTS = 1;
	studentRec student[TOTAL_STUDENTS];

    for ( int i = 0; i < TOTAL_STUDENTS; i++) {
        student[i] = getStudentData();
    }

	calculateStuGrade (student[0]);
		  
	outputStuData (student[0]);

	outputAveExams (student,TOTAL_STUDENTS);

	outputTotalGrades (student,TOTAL_STUDENTS);

	return 0;

}

/*
void getStudentData ( studentRec student [], int arraySize)
{
	for (int x=0; x < arraySize ; x++)
	{
		
		
		cout << "Enter Student's Full Name : ";
		cin.getline(student[x].name, MAX_NAME_SIZE);

		cout << "Enter Home Work 1 Score (range from 0 to 25): ";
		cin >> student[x].hw1;

		cout << "Enter Home Work 2 Score (range from 0 to 25): ";
		cin >> student[x].hw2;

		cout << "Enter Home Work 3 Score (range from 0 to 25): ";
		cin >> student[x].hw3;

		cout << "Enter Exam 1 Score (range from 0 to 100): ";
		cin >> student[x].exam1;

		cout << "Enter Exam 2 Score (range from 0 to 100): ";
		cin >> student[x].exam2;
		cout <<endl;

		cin.ignore(1000,'\n');
	
	}
}
*/

studentRec getStudentData() {

    studentRec student;
    bool proceed = true;

    cout << "Enter student's full name : ";
    cin.getline(student.name,MAX_NAME_SIZE);

    for(int cont = 0;cont < MAX_HW_SIZE; cont++) {
        cout << "Enter homework " << cont << " score ";
        cin >> student.hw[cont];
        cout << endl;
    }

    for (int cont = 0 ; cont < MAX_EXAM_SIZE; cont++) {
        cout << "Enter exam " << cont << " score " ;
        cin >> student.exam[cont];
        cout << endl;
    }

    cin.ignore(BUFFER_SIZE,'\n');
    return student;
}

/*
void calculateStuGrade (studentRec student [], int arraySize )
{
	for (int k=0; k < arraySize ; k++)
	{
		student[k].totalHomeWkPts = student[k].hw1 + student[k].hw2 + student[k].hw3;
		student[k].totalexamPts = student[k].exam1 + student[k].exam2 ;

		student[k].classPert = (student[k].totalHomeWkPts / 1.875) + (student[k].totalexamPts/3.333);

		if (student[k].classPert >= 90)
			student[k].grade = 'A';

		else if (student[k].classPert < 90 && student[k].classPert >= 80)
			student[k].grade = 'B';

		else if (student[k].classPert < 80 && student[k].classPert >= 70)
			student[k].grade = 'C';

		else if (student[k].classPert < 70 && student[k].classPert >= 60)
			student[k].grade = 'D';

		else
			student[k].grade = 'F';
		
	
	}
  }
*/

void calculateStuGrade(studentRec &student) {
    int loop = 0;
    while ( loop < MAX_HW_SIZE || loop < MAX_EXAM_SIZE) {

        if(loop < MAX_HW_SIZE){
            student.totalHomeWkPts += student.hw[loop];
        }
        if( loop < MAX_EXAM_SIZE){
            student.totalExamPts += student.exam[loop];
        }
        loop++;
    }
    student.classPert = (student.totalHomeWkPts / 1.875) + (student.totalExamPts/3.333);

		if (student.classPert >= 90)
			student.grade = 'A';
		else if (student.classPert < 90 && student.classPert >= 80)
			student.grade = 'B';

		else if (student.classPert < 80 && student.classPert >= 70)
			student.grade = 'C';

		else if (student.classPert < 70 && student.classPert >= 60)
			student.grade = 'D';
		else
			student.grade = 'F';
    
}

void outputStuData (studentRec &student)
{
	cout << left;
	cout << setw(20) << "Student' Name";
	cout << setw(15) << "Total HW Pts";
	cout << setw(18) << "Total Exam Pts";
	cout << setw(15) << "Class Perct.";
	cout << setw(15) << "Letter Grade" << endl;
	cout << right;

		cout << setprecision(2) << showpoint << fixed;
		cout << left << setw(20) << student.name;
		cout << setw(15) << student.totalHomeWkPts;
		cout << setw(18) << student.totalExamPts;
		cout << setw(15) << student.classPert;
		cout << setw (15) << student.grade << right << endl;
}

void outputAveExams (studentRec student [], int totalStudent)
{
	float totalExam1 = 0.0;
	float totalExam2 = 0.0;

	for ( int m = 0; m < totalStudent; m++ )
	{
		
	}

	cout << endl ;
	cout << setprecision(2) << showpoint << fixed;
	cout <<  "Average Exam 1 Score: " <<  totalExam1 / ( totalStudent + 0.0 ) << endl;
	cout << "Average Exam 2 Score: " <<  totalExam2 / ( totalStudent + 0.0 ) << endl << endl;
}

void outputTotalGrades (studentRec student [], int totalStudent)
{
	int gradeA =0;
	int gradeB =0;
	int gradeC =0;
	int gradeD =0;
	int gradeF =0;

	
	for ( int j = 0; j < totalStudent; j++ )
	{
		switch (student[j].grade)
		{
			case 'A' : gradeA++;
						break;

			case 'B' : gradeB++;
						break;
			case 'C' : gradeC++;
						break;
			case 'D' : gradeD++;
						break;
			case 'F' : gradeF++;
						break;
		}
	}

    cout << "Total letter grade A's are: " << gradeA << endl;
	cout << "Total letter grade B's are: " << gradeB << endl;
	cout << "Total letter grade C's are: " << gradeC << endl;
	cout << "Total letter grade D's are: " << gradeD << endl;
	cout << "Total letter grade F's are: " << gradeF << endl;

}

