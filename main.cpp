#include <iostream>
#include <iomanip>
using namespace std;

//Prototypes
struct studentRec
{
		                        
		char name[50];
		int hw1, hw2, hw3;
		int exam1, exam2;
		float totalHomeWkPts;
		float totalexamPts;
		float classPert;
		char grade;
};

void getStudentData ( studentRec student [], int arraySize);
void calculateStuGrade (studentRec student [], int arraySize );
void outputStuData (studentRec student [], int index);
void outputAveExams (studentRec student [], int totalStudent);
void outputTotalGrades (studentRec student [], int totalStudent);

int main()

{
	const int totalStudent = 5;
	studentRec student[totalStudent];

	getStudentData (student, totalStudent);

	calculateStuGrade (student, totalStudent);
		  
	outputStuData (student, totalStudent);

	outputAveExams (student, totalStudent);

	outputTotalGrades (student, totalStudent);

	system("PAUSE");

	return 0;

}

void getStudentData ( studentRec student [], int arraySize)
{
	for (int x=0; x < arraySize ; x++)
	{
		
		
		cout << "Enter Student's Full Name : ";
		cin.getline(student[x].name, 50);

		cout << "Enter Home Work 1 Score (range from 0 to 25): ";
		cin >> student[x].hw1;

		cout << "Enter Home Work 2 Score (range from 0 to 25): ";
		cin >> student[x].hw2;

		cout << "Enter Home Work 3 Score (range from 0 to 25): ";
		cin >> student[x].hw3;

		cout << "Enter Exam 1 Score (range from 0 to 100): ";
		cin >> student[x].exam1;

		cout << "Enter Exam 2 Score (range from 0 to 100): ";
		cin >> student[x].exam2;
		cout <<endl;

		cin.ignore(1000,'\n');
	
	}
}

void calculateStuGrade (studentRec student [], int arraySize )
{
	for (int k=0; k < arraySize ; k++)
	{
		student[k].totalHomeWkPts = student[k].hw1 + student[k].hw2 + student[k].hw3;
		student[k].totalexamPts = student[k].exam1 + student[k].exam2 ;

		student[k].classPert = (student[k].totalHomeWkPts / 1.875) + (student[k].totalexamPts/3.333);

		if (student[k].classPert >= 90)
			student[k].grade = 'A';

		else if (student[k].classPert < 90 && student[k].classPert >= 80)
			student[k].grade = 'B';

		else if (student[k].classPert < 80 && student[k].classPert >= 70)
			student[k].grade = 'C';

		else if (student[k].classPert < 70 && student[k].classPert >= 60)
			student[k].grade = 'D';

		else
			student[k].grade = 'F';
		
	
	}


}

void outputStuData (studentRec student [], int index)
{
	cout << left;
	cout << setw(20) << "Student' Name";
	cout << setw(15) << "Total HW Pts";
	cout << setw(18) << "Total Exam Pts";
	cout << setw(15) << "Class Perct.";
	cout << setw(15) << "Letter Grade" << endl;
	cout << right;

	for ( int k = 0; k < index; k++ )
	{
		cout << setprecision(2) << showpoint << fixed;
		cout << left << setw(20) << student[k].name;
		cout << setw(15) << student[k].totalHomeWkPts;
		cout << setw(18) << student[k].totalexamPts;
		cout << setw(15) << student[k].classPert;
		cout << setw (15) << student[k].grade << right << endl;

	}

}

void outputAveExams (studentRec student [], int totalStudent)
{
	float totalExam1 = 0.0;
	float totalExam2 = 0.0;

	for ( int m = 0; m < totalStudent; m++ )
	{
		totalExam1 = totalExam1 + student[m].exam1;
		totalExam2 = totalExam2 + student[m].exam2;
		
	}

	cout << endl ;
	cout << setprecision(2) << showpoint << fixed;
	cout <<  "Average Exam 1 Score: " <<  totalExam1 / ( totalStudent + 0.0 ) << endl;
	cout << "Average Exam 2 Score: " <<  totalExam2 / ( totalStudent + 0.0 ) << endl << endl;
}

void outputTotalGrades (studentRec student [], int totalStudent)
{
	int gradeA =0;
	int gradeB =0;
	int gradeC =0;
	int gradeD =0;
	int gradeF =0;

	
	for ( int j = 0; j < totalStudent; j++ )
	{
		switch (student[j].grade)
		{
			case 'A' : gradeA++;
						break;

			case 'B' : gradeB++;
						break;
			case 'C' : gradeC++;
						break;
			case 'D' : gradeD++;
						break;
			case 'F' : gradeF++;
						break;
		}
	}

    cout << "Total latter garde A's are: " << gradeA << endl;
	cout << "Total latter garde B's are: " << gradeB << endl;
	cout << "Total latter garde C's are: " << gradeC << endl;
	cout << "Total latter garde D's are: " << gradeD << endl;
	cout << "Total latter garde F's are: " << gradeF << endl;

}

